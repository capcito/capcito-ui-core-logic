import * as Pkg from '../../src/index'

describe('Factoring', () => {
  it('exports property Factoring', () => {
    expect(Pkg).toHaveProperty('Factoring')
  })

  it('will calculate fee', () => {
    expect(Pkg.Factoring.fee(100_000, 0.02))
      .toBe(2000)
  })

  it('will calculate fee with vat', () => {
    expect(Pkg.Factoring.fee(100_000, 0.02, true))
      .toBe(2500)
  })
})

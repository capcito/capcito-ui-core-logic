import * as Pkg from '../../src/flex'

describe('Flex', () => {
  it('exports property Flex', () => {
    expect(Pkg).toHaveProperty('Flex')
  })

  it('will calculate cost', () => {
    expect(Pkg.Flex.cost(100_000, 0.01, 30))
      .toBe(1000)

    expect(Pkg.Flex.cost(100_000, 0.01, 60))
      .toBe(2000)

    expect(Pkg.Flex.cost(100_000, 0.01, 120))
      .toBe(4000)
  })
})

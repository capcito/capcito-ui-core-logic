import * as Pkg from '../../src/index'

describe('Month', () => {
  it('exports property Month', () => {
    expect(Pkg).toHaveProperty('Month')
  })

  it('will convert into weeks', () => {
    expect(Pkg.Month.toWeeks(1))
      .toBe(4)

    expect(Pkg.Month.toWeeks(12))
      .toBe(52)
  })

  it('will convert weeks into months', () => {
    expect(Pkg.Month.toMonths(4)).toBe(1)
    expect(Pkg.Month.toMonths(8)).toBe(2)
    expect(Pkg.Month.toMonths(12)).toBe(3)
    expect(Pkg.Month.toMonths(16)).toBe(4)
    expect(Pkg.Month.toMonths(20)).toBe(5)
    expect(Pkg.Month.toMonths(24)).toBe(6)
    expect(Pkg.Month.toMonths(28)).toBe(7)
    expect(Pkg.Month.toMonths(32)).toBe(8)
    expect(Pkg.Month.toMonths(36)).toBe(9)
    expect(Pkg.Month.toMonths(40)).toBe(10)
    expect(Pkg.Month.toMonths(44)).toBe(11)
    expect(Pkg.Month.toMonths(48)).toBe(12)
    expect(Pkg.Month.toMonths(52)).toBe(12)
  })
})

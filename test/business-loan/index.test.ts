import * as Pkg from '../../src/index'

describe('Business loan', () => {
  it('exports property BusinessLoan', () => {
    expect(Pkg).toHaveProperty('BusinessLoan')
  })

  it('will calculate annuity', () => {
    const result = Pkg.BusinessLoan.annuity(100_000, 0.02, 52)

    expect(Math.round(result)).toBe(2170)
  })

  it('will calculate total cost', () => {
    const result = Pkg.BusinessLoan.cost(100_000, 0.02, 52)

    expect(Math.round(result)).toBe(112_856)
  })

  it('will calculate monthly cost for fee based customers', () => {
    expect(Pkg.BusinessLoan.feeBased(100_000, 0.02, 26).monthly.cost)
      .toBe(18_670)

    expect(Pkg.BusinessLoan.feeBased(100_000, 0.025, 26).monthly.cost)
      .toBe(19_170)
  })

  it('will calculate monthly amortization and fee as total cost', () => {
    const { fee, amortization, cost } = Pkg.BusinessLoan.feeBased(100_000, 0.02, 26).monthly

    // Making sure to round it to closest base 10
    expect(amortization).toBe(16_667)
    expect(fee).toBe(2_000)

    expect(Math.round((amortization + fee) / 10) * 10).toBe(cost)
  })
})

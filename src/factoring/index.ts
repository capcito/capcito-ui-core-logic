const fee = (amount: number, interestRate: number, vat: boolean = false) => {
  return (amount * interestRate * (vat ? 1.25 : 1))
}

export const Factoring = {
  fee
}

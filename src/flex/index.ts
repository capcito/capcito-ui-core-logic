const cost = (amount: number, interestRate: number, period: number) => {
  return amount * interestRate * period / 30
}

export const Flex = {
  cost
}

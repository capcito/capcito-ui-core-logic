import { Month } from '../utils/index'

const annuity = (amount: number, interestRate: number, repaymentWeeks: number, repaymentInterval = 1, repaymentPeriod = 7) => {
  const weeklyInterestRate = ((interestRate / 30) * (repaymentPeriod * repaymentInterval))
  const negRepaymentWeeks = (-1 * repaymentWeeks / repaymentInterval)

  return (amount * weeklyInterestRate) / (1 - (Math.pow((1 + weeklyInterestRate), negRepaymentWeeks)))
}

const cost = (amount: number, interestRate: number, repaymentWeeks: number, repaymentInterval = 1, repaymentPeriod = 7) => {
  return annuity(amount, interestRate, repaymentWeeks, repaymentInterval, repaymentPeriod) * (repaymentWeeks / repaymentInterval)
}

const feeBased = (amount: number, feeRate: number, repaymentWeeks: number, repaymentInterval = 1) => {
  const numberOfPeriods = Month.toMonths(repaymentWeeks) / repaymentInterval
  const monthlyFeeCost = feeRate * amount
  const totalFeeCost = monthlyFeeCost * numberOfPeriods

  function round (value: number): number {
    if (value > 1000) {
      return Math.round(value / 10) * 10
    }

    return value
  }

  const result = Math.ceil((amount + totalFeeCost) / numberOfPeriods)

  return {
    monthly: {
      amortization: Math.ceil(amount / numberOfPeriods),
      fee: monthlyFeeCost,
      cost: round(result)
    },
    total: {
      cost: round(result) * numberOfPeriods,
      fee: totalFeeCost
    }
  }
}

export const BusinessLoan = {
  feeBased,
  annuity,
  cost
}

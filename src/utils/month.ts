const toWeeks = (months: number) => {
  return Math.round(30.3 * months / 7)
}

const toMonths = (weeks: number) => {
  return Math.ceil(weeks / 4.34524)
}

export const Month = {
  toMonths,
  toWeeks
}
